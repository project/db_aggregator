<?php

/**
 * @file
 * Functions to synchronize data.
 */

use Drupal\Core\Database\Connection;
use Drupal\db_aggregator\Form\AggregationTableMappingForm;
use Drupal\node\Entity\Node;
use Drupal\db_aggregator\AggregationTableMappingInterface;
use Drupal\db_aggregator\Entity\AggregationDataSource;
use Drupal\db_aggregator\Entity\AggregationTableMapping;
use Drupal\Core\Database\Database;

/**
 * Create a database connection.
 */
function synchronize_connection(AggregationDataSource $data_source): Connection {
  $driver_namespace = $data_source->get('type') === 'oracle' ?
    'Drupal\\oracle\\Driver\\Database\\oracle' :
    'Drupal\\Core\\Database\\Driver\\mysql';
  $database = [
    'database' => $data_source->get('database'),
    'username' => $data_source->get('username'),
    'password' => $data_source->get('password'),
    'prefix' => $data_source->get('prefix'),
    'host' => $data_source->get('host'),
    'port' => $data_source->get('port'),
    'namespace' => $driver_namespace,
    'driver' => $data_source->get('type'),
  ];
  Database::addConnectionInfo($data_source->id(), 'default', $database);
  return Database::getConnection('default', $data_source->id());
}

/**
 * Get datasource fiels to fetch.
 */
function synchronize_get_datasource_fields(AggregationTableMapping $table_mapping): array {
  $data_source = $table_mapping->getDataSource();
  // Determine what fields to fetch.
  $fields = [$data_source->get('primaryKey')];
  if ($data_source->get('timestampField')) {
    $fields[] = $data_source->get('timestampField');
  }
  if ($data_source->get('statusField')) {
    $fields[] = $data_source->get('statusField');
  }
  foreach ($table_mapping->get('mappings') as $mapping) {
    $fields[] = $mapping['sourceField'];
  }
  return $fields;
}

/**
 * Synchronize a batch of rows.
 *
 * @throws \Drupal\Core\Entity\EntityStorageException
 * @throws \Drupal\Component\Plugin\Exception\PluginException
 */
function synchronize_rows(AggregationTableMapping $table_mapping, array $ids, &$context = []) {
  // Fetch current batch of rows.
  $data_source = $table_mapping->getDataSource();
  $connect = synchronize_connection($data_source);
  $result = $connect
    ->select($data_source->get('table'), 't')
    ->fields('t', synchronize_get_datasource_fields($table_mapping))
    ->condition($data_source->get('primaryKey'), $ids, 'IN')
    ->execute();
  $status_string = substr(implode(',', $ids), 0, 100) . '...';
  foreach ($result->fetchAllAssoc('id', PDO::FETCH_ASSOC) as $key => $row) {
    synchronize_row($key,
      empty($data_source->get('timestampField')) ? NULL : $row[$data_source->get('timestampField')],
      empty($data_source->get('statusField')) ? NULL : $row[$data_source->get('statusField')],
      $row,
      $table_mapping);
    $context['results'][] = t('Processing rows of :keys', [':keys' => $status_string]);
    $context['message'] = t('Processing rows of :keys', [':keys' => $status_string]);
  }
}

/**
 * Synchronize a row.
 *
 * @throws \Drupal\Core\Entity\EntityStorageException
 * @throws \Drupal\Component\Plugin\Exception\PluginException
 */
function synchronize_row($key, $timestamp, $status, $row, AggregationTableMappingInterface $table_mapping) {
  // Find nodes.
  $node_storage = \Drupal::entityTypeManager()->getStorage('node');
  $row_identifier_data = [
    'type' => $table_mapping->get('contentType'),
    $table_mapping->get('sourceTrackingField') => $table_mapping->id() . ':' . $key,
  ];
  $nodes = $node_storage->loadByProperties($row_identifier_data);
  if ($table_mapping->get('synchronizeType')
    === AggregationTableMappingForm::SYNCHRONIZE_TYPE_CLEAN_INSERT) {
    $node_storage->delete($nodes);
  }
  if (empty($nodes)) {
    $node_data = $row_identifier_data;
    foreach ($table_mapping->get('mappings') as $mapping) {
      $node_data[$mapping['targetField']][$mapping['targetProperty'] ?? 'value']
        = $row[$mapping['sourceField']];
    }
    $nodes = [
      Node::create($node_data),
    ];
  }
  /** @var \Drupal\db_aggregator\DbSynchronizerPluginManager $manager */
  $manager = \Drupal::service('plugin.manager.db_synchronizer');
  /** @var \Drupal\db_aggregator\DbSynchronizerInterface $synchronizer */
  $synchronizer = $manager->createInstance($table_mapping->get('synchronizer'));
  $synchronizer->synchronize((int) $key, (int) $timestamp, $status, $nodes, $table_mapping->get('synchronyType'));
}

/**
 * Callback for synchronization task is complete.
 */
function synchronize_finished_callback($success, $results, $operations, $elapsed) {
  // See callback_batch_finished() for more information about these
  // parameters.
  // The 'success' parameter means no fatal PHP errors were detected. All
  // other error management should be handled using 'results'.
  if ($success) {
    $message = \Drupal::translation()
      ->formatPlural(count($results), 'row post processed.', '@count rows processed.');
  }
  else {
    $message = t('Finished with an error.');
  }
  \Drupal::messenger()
    ->addMessage($message);
}
