<?php

namespace Drupal\db_aggregator\Form;

use Drupal\Core\Database\StatementInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Renderer;
use Drupal\db_aggregator\AggregationDataSourceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Oracle data source form.
 *
 * @property \Drupal\db_aggregator\AggregationDataSourceInterface $entity
 */
class AggregationDataSourceForm extends EntityForm {

  const DATABASE_TYPE_MYSQL = 'mysql';
  const DATABASE_TYPE_ORACLE = 'oracle';

  /**
   * The Renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  private $renderer;

  /**
   * Construct form.
   */
  public function __construct(Renderer $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container): AggregationDataSourceForm {
    return new static(
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Label for the aggregation data source.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\db_aggregator\Entity\AggregationDataSource::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $driver_options = [self::DATABASE_TYPE_MYSQL => $this->t('MySQL')];
    if (class_exists('\Drupal\oracle\Driver\Database\oracle\Connection')) {
      $driver_options[self::DATABASE_TYPE_ORACLE] = $this->t('Oracle');
    }
    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#options' => $driver_options,
      '#default_value' => $this->entity->get('type'),
      '#description' => $this->t('Database type for the aggregation data source.'),
      '#required' => TRUE,
    ];

    $form['database'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Database'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->get('database'),
      '#description' => $this->t('Database name for the aggregation data source.'),
      '#required' => TRUE,
    ];

    $form['table'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Table'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->get('table'),
      '#description' => $this->t('Table name for the aggregation data source.'),
      '#required' => TRUE,
    ];

    $form['primaryKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Primary key'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->get('primaryKey'),
      '#description' => $this->t('primary key of the aggregation data source table.'),
      '#required' => TRUE,
    ];

    $form['timestampField'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Timestamp field'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->get('timestampField'),
      '#description' => $this->t('Timestamp field of the aggregation data source table, leave it empty unless you plan to use the "Check timestamp and status" synchronizer in table mapping form.'),
      '#required' => FALSE,
    ];

    $form['statusField'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Status field'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->get('statusField'),
      '#description' => $this->t('Status field of the aggregation data source table, leave it empty unless you plan to use the "Check timestamp and status" synchronizer in table mapping form.'),
      '#required' => FALSE,
    ];

    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->get('username'),
      '#description' => $this->t('Username for the aggregation data source.'),
      '#required' => TRUE,
    ];

    $form['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->get('password'),
      '#description' => $this->t('Password for the aggregation data source.'),
      '#required' => TRUE,
    ];

    $form['prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prefix'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->get('prefix'),
      '#description' => $this->t('Prefix for the aggregation data source, normally empty.'),
      '#required' => FALSE,
    ];

    $form['host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->get('host'),
      '#description' => $this->t('Host for the aggregation data source.'),
      '#required' => TRUE,
    ];

    $form['port'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Port'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->get('port'),
      '#description' => $this->t('Port for the aggregation data source, normally 3306 for MySQL, 1521 for Oracle.'),
      '#required' => TRUE,
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
      '#description' => $this->t('If enable this data source in table mapping form.'),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
      '#description' => $this->t('Description of this aggregation data source.'),
    ];

    $this->renderer->addCacheableDependency($form, $this->entity);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    // Turn database identifier into lowercase, when it's using Oracle.
    if ($form_state->getValue('type') === static::DATABASE_TYPE_ORACLE) {
      $form_state->setValue('database', strtolower($form_state->getValue('database')));
      $form_state->setValue('table', strtolower($form_state->getValue('table')));
      $form_state->setValue('prefix', strtolower($form_state->getValue('prefix')));
      $form_state->setValue('primaryKey', strtolower($form_state->getValue('primaryKey')));
      $form_state->setValue('timestampField', strtolower($form_state->getValue('timestampField')));
      $form_state->setValue('statusField', strtolower($form_state->getValue('statusField')));
      $this->messenger()->addWarning($this->t("Turn database identifier into lowercase, when it's using Oracle."));
    }
    // Make a database connection test.
    include_once __DIR__ . '/../../synchronize.inc';
    $form_state->cleanValues();
    $entity = $this->buildEntity($form, $form_state);
    if ($entity instanceof AggregationDataSourceInterface) {
      try {
        $connection = synchronize_connection($entity);
        $statement = $connection
          ->select($entity->get('table'))
          ->countQuery()
          ->execute();
        if ($statement instanceof StatementInterface) {
          $num_rows = $statement
            ->fetchField();
        }
        else {
          $form_state->setError($form, $this->t('Can not query on table %table', [
            '%table' => $entity->get('table'),
          ]));
        }
      }
      catch (\Exception $exception) {
        $form_state->setError($form, $exception->getMessage());
      }
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
      ? $this->t('Created new aggregation data source %label.', $message_args)
      : $this->t('Updated aggregation data source %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
