<?php

namespace Drupal\db_aggregator\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\NodeType;
use Drupal\db_aggregator\Entity\AggregationDataSource;

/**
 * Aggregation table mapping form.
 *
 * @property \Drupal\db_aggregator\AggregationTableMappingInterface $entity
 */
class AggregationTableMappingForm extends EntityForm {

  const SYNCHRONIZE_TYPE_CLEAN_INSERT = 'clean_insert';
  const SYNCHRONIZE_TYPE_UPDATE_ONLY = 'update_only';
  const SYNCHRONIZE_TYPE_INSERT_ONLY = 'insert_only';
  const SYNCHRONIZE_TYPE_UPDATE_AND_INSERT = 'update_and_insert';

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $default_values = [
      'label' => NULL,
      'id' => NULL,
      'datasource' => array_key_first($this->getDatasourceOptions()),
      'contentType' => array_key_first($this->getContentTypeOptions()),
      'sourceTrackingField' => NULL,
      'mappings' => [
        [
          'sourceField' => '',
          'targetField' => '',
          'targetProperty' => '',
        ],
      ],
      'synchronizer' => 'simple',
      'synchronyTime' => 0,
      'synchronyType' => self::SYNCHRONIZE_TYPE_UPDATE_AND_INSERT,
      'status' => TRUE,
      'description' => NULL,
    ];
    if (empty($default_values['datasource']) || empty($default_values['contentType'])) {
      $form['msg'] = [
        '#markup' => '<p>' . $this->t('There are no Aggregation Data Sources or Content Types, please add them first.') . '</p>',
      ];
      return $form;
    }
    if (!$this->entity->isNew()) {
      $default_values = [
        'label' => $this->entity->label(),
        'id' => $this->entity->id(),
        'datasource' => $this->entity->get('datasource'),
        'contentType' => $this->entity->get('contentType'),
        'sourceTrackingField' => $this->entity->get('sourceTrackingField'),
        'mappings' => $this->entity->get('mappings'),
        'synchronizer' => $this->entity->get('synchronizer'),
        'synchronyTime' => $this->entity->get('synchronyTime'),
        'synchronyType' => $this->entity->get('synchronyType'),
        'status' => $this->entity->get('status'),
        'description' => $this->entity->get('description'),
      ] + $default_values;
    }
    if (!empty($form_state->getUserInput())) {
      $default_values = $form_state->getUserInput() + $default_values;
    }
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $default_values['label'],
      '#description' => $this->t('Label for the aggregation table mapping.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $default_values['id'],
      '#machine_name' => [
        'exists' => '\Drupal\db_aggregator\Entity\AggregationTableMapping::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['datasource'] = [
      '#type' => 'select',
      '#title' => $this->t('Datasource'),
      '#options' => $this->getDatasourceOptions(),
      '#default_value' => $default_values['datasource'],
      '#description' => $this->t('Datasource for the aggregation table mapping.'),
      '#required' => TRUE,
    ];

    $form['contentType'] = [
      '#type' => 'select',
      '#title' => $this->t('Content type'),
      '#options' => $this->getContentTypeOptions(),
      '#default_value' => $default_values['contentType'],
      '#description' => $this->t('Content type for the aggregation table mapping.'),
      '#required' => TRUE,
    ];

    $form['sourceTrackingField'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Source tracking field'),
      '#maxlength' => 255,
      '#default_value' => $default_values['sourceTrackingField'],
      '#description' => $this->t('Field name on the content type to store identifier of source data rows.'),
      '#required' => TRUE,
    ];

    $wrapper_id = Html::getUniqueId('add-more-mapping-wrapper');
    $form['mappings_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Field mappings'),
      '#attributes' => [
        'id' => $wrapper_id,
      ],
    ];
    $form['mappings_wrapper']['mappings'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Source field'),
        $this->t('Target field'),
        $this->t('Target property'),
        $this->t('Operations'),
      ],
      '#tree' => TRUE,
    ];

    foreach ($default_values['mappings'] as $i => $mapping) {
      $form['mappings_wrapper']['mappings'][$i] = self::getMappingRow($wrapper_id, $i, $mapping);
    }

    $form['mappings_wrapper']['add_mapping'] = [
      '#type' => 'button',
      '#value' => $this->t('Add mapping item'),
      '#add_btn' => TRUE,
      '#ajax' => [
        'callback' => [AggregationTableMappingForm::class, 'ajaxRefresh'],
        'wrapper' => $wrapper_id,
        'effect' => 'fade',
      ],
      '#limit_validation_errors' => [],
    ];

    $form['synchronizer'] = [
      '#type' => 'select',
      '#title' => $this->t('Synchronizer'),
      '#options' => (function () {
        $options = [];
        /** @var \Drupal\db_aggregator\DbSynchronizerPluginManager $manager */
        $manager = \Drupal::service('plugin.manager.db_synchronizer');
        foreach ($manager->getDefinitions() as $key => $definition) {
          $options[$key] = $definition['label'];
        }
        return $options;
      })(),
      '#default_value' => $default_values['synchronizer'],
      '#description' => $this->t('How synchronize to a row, the datasource must provides timestamp field and status field if you select "Check timestamp and status".'),
      '#required' => TRUE,
    ];

    $form['synchronyTime'] = [
      '#type' => 'select',
      '#title' => $this->t('Synchrony time'),
      '#options' => (function () {
        $options = [];
        for ($hour = 0; $hour < 24; $hour++) {
          $options[$hour] = t('at :h clock', [':h' => $hour]);
        }
        return $options;
      })(),
      '#default_value' => $default_values['synchronyTime'],
      '#description' => $this->t('When synchronize automatically.'),
      '#required' => TRUE,
    ];
    $form['synchronyType'] = [
      '#type' => 'radios',
      '#title' => $this->t('Synchrony type'),
      '#options' => [
        self::SYNCHRONIZE_TYPE_CLEAN_INSERT => t('Clean and insert'),
        self::SYNCHRONIZE_TYPE_UPDATE_ONLY => t('Update existed only'),
        self::SYNCHRONIZE_TYPE_INSERT_ONLY => t('Insert new only'),
        self::SYNCHRONIZE_TYPE_UPDATE_AND_INSERT => t('Update existed and insert new'),
      ],
      '#default_value' => $default_values['synchronyType'],
      '#description' => $this->t('How synchronize.'),
      '#required' => TRUE,
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $default_values['status'],
      '#description' => $this->t('Enable automatically synchronize with cron.'),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $default_values['description'],
      '#description' => $this->t('Description of the aggregation table mapping.'),
    ];

    $renderer = \Drupal::service('renderer');
    $renderer->addCacheableDependency($form, $this->entity);
    $dataSources = AggregationDataSource::loadMultiple();
    foreach ($dataSources as $dataSource) {
      $renderer->addCacheableDependency($form, $dataSource);
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    if (empty($this->getDatasourceOptions()) || empty($this->getContentTypeOptions())) {
      return [];
    }
    $actions = parent::actions($form, $form_state);
    $actions['synchronize'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save and Synchronize immediately'),
      '#button_type' => 'secondary',
      '#submit' => $actions['submit']['#submit'],
      '#synchronize' => TRUE,
    ];
    return $actions;
  }

  /**
   * Get Datasource config entity list for datasource property field.
   */
  private function getDatasourceOptions(): array {
    $dataSources = AggregationDataSource::loadMultiple();
    $options = [];
    foreach ($dataSources as $dataSource) {
      $options[$dataSource->id()] = $dataSource->label();
    }
    return $options;
  }

  /**
   * Get contentType entity list for contentType property field.
   */
  private function getContentTypeOptions(): array {
    $content_types = NodeType::loadMultiple();
    $options = [];
    foreach ($content_types as $content_type) {
      $options[$content_type->id()] = $content_type->label();
    }
    return $options;
  }

  /**
   * Ajax refreshes for mapping list.
   */
  public static function ajaxRefresh($form, FormStateInterface $form_state) {
    return $form['mappings_wrapper'];
  }

  /**
   * Validate the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $trigger_element = $form_state->getTriggeringElement();
    if ($trigger_element && (isset($trigger_element['#add_btn']) || isset($trigger_element['#remove_btn']))) {
      $inputs = $form_state->getUserInput();
      if (isset($trigger_element['#add_btn'])) {
        $inputs['mappings'][] = [
          'sourceField' => '',
          'targetField' => '',
          'targetProperty' => '',
        ];
      }
      if (isset($trigger_element['#remove_btn'])) {
        unset($inputs['mappings'][$trigger_element['#data_index']]);
      }
      $form_state->setUserInput($inputs);
      $form_state->setRebuild();
    }

    // Turn database identifier into lowercase, when it's using Oracle.
    if (AggregationDataSource::load($form_state->getValue('datasource'))->get('type')
      === AggregationDataSourceForm::DATABASE_TYPE_ORACLE) {
      $mappings = $form_state->getValue('mappings');
      foreach ($mappings as $key => $mapping) {
        $mappings[$key]['sourceField'] = strtolower($mapping['sourceField']);
      }
      $form_state->setValue('mappings', $mappings);
      $this->messenger()->addWarning($this->t("Turn database identifier into lowercase, when it's using Oracle."));
    }
  }

  /**
   * Generate mapping row.
   *
   * @param string $wrapper_id
   *   The wrapper id.
   * @param int $i
   *   The row index.
   * @param array $values
   *   Initial values.
   *
   * @return array[]
   *   Render array.
   */
  public static function getMappingRow(string $wrapper_id, int $i, array $values): array {
    return [
      'sourceField' => [
        '#type' => 'textfield',
        '#size' => 30,
        '#title' => t('Source field'),
        '#title_display' => 'hidden',
        '#placeholder' => t('Make sure this field name exist in source table.'),
        '#default_value' => $values['sourceField'],
      ],
      'targetField' => [
        '#type' => 'textfield',
        '#size' => 30,
        '#title' => t('Target field'),
        '#title_display' => 'hidden',
        '#default_value' => $values['targetField'],
      ],
      'targetProperty' => [
        '#type' => 'textfield',
        '#size' => 30,
        '#title' => t('Target property'),
        '#title_display' => 'hidden',
        '#placeholder' => t('Normally set "value" as the main property here.'),
        '#default_value' => $values['targetProperty'],
      ],
      'remove' => [
        '#type' => 'button',
        '#value' => t('Remove'),
        '#remove_btn' => TRUE,
        '#data_index' => $i,
        '#name' => 'remove_btn_' . $i,
        '#ajax' => [
          'callback' => [AggregationTableMappingForm::class, 'ajaxRefresh'],
          'wrapper' => $wrapper_id,
          'effect' => 'fade',
        ],
        '#limit_validation_errors' => [],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $trigger_element = $form_state->getTriggeringElement();
    if (isset($trigger_element['#synchronize'])) {
      try {
        $this->runSynchronization();
      }
      catch (\Exception $exception) {
        \Drupal::messenger()->addError($this->t('Synchronization fails with error: %msg', ['%msg' => $exception->getMessage()]));
      }
    }

    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
      ? $this->t('Created new aggregation table mapping %label.', $message_args)
      : $this->t('Updated aggregation table mapping %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

  /**
   * Run synchronization immediately.
   */
  private function runSynchronization() {
    $batch = [
      'title' => t('Synchronizing from datasource'),
      'operations' => _db_aggregator_prepare_operations_for_batch(
        $this->entity,
        (int) \Drupal::config('db_aggregator.settings')->get('batch_size')),
      'finished' => 'synchronize_finished_callback',
      'file' => drupal_get_path('module', 'db_aggregator') . '/synchronize.inc',
    ];
    batch_set($batch);
  }

}
