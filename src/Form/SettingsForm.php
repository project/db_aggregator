<?php

namespace Drupal\db_aggregator\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure DB Aggregator settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'db_aggregator_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['db_aggregator.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['batch_size'] = [
      '#type' => 'number',
      '#title' => $this->t('Batch size'),
      '#description' => $this->t('How many row to synchronize per batch operation, max limited at 1000.'),
      '#min' => 1,
      '#max' => 1000,
      '#step' => 1,
      '#default_value' => $this->config('db_aggregator.settings')->get('batch_size'),
    ];
    $renderer = \Drupal::service('renderer');
    $renderer->addCacheableDependency($form, $this->config('db_aggregator.settings'));
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ((int) $form_state->getValue('batch_size') > 1000) {
      $form_state->setErrorByName('batch_size', $this->t('The batch size is too large.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('db_aggregator.settings')
      ->set('batch_size', $form_state->getValue('batch_size'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
