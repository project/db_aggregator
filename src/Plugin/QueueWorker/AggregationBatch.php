<?php

namespace Drupal\db_aggregator\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Defines 'db_aggregator_aggregation_batch' queue worker.
 *
 * @QueueWorker(
 *   id = "db_aggregator_aggregation_batch",
 *   title = @Translation("Aggregation Batch"),
 *   cron = {"time" = 60}
 * )
 */
class AggregationBatch extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    \Drupal::logger('db_aggregator')->info(t('Begin processItem.'));
    \Drupal::logger('db_aggregator')->info(var_export($data, TRUE));
    if (is_callable($data[0])) {
      /** @var \Drupal\db_aggregator\Entity\AggregationTableMapping $table_mapping */
      $table_mapping = $data[1][0];
      \Drupal::logger('db_aggregator')->info(t('Processing queued operation of aggregation :id.', [':id' => $table_mapping->id()]));
      call_user_func_array($data[0], $data[1]);
    }
    \Drupal::logger('db_aggregator')->info(t('Finished processItem.'));
  }

}
