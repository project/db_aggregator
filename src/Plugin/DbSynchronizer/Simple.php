<?php

namespace Drupal\db_aggregator\Plugin\DbSynchronizer;

use Drupal\db_aggregator\DbSynchronizerPluginBase;
use Drupal\db_aggregator\Form\AggregationTableMappingForm;

/**
 * Plugin implementation of the db_synchronizer.
 *
 * @DbSynchronizer(
 *   id = "simple",
 *   label = @Translation("Simple"),
 *   description = @Translation("Simplly check source id to indecate if data row is existe.")
 * )
 */
class Simple extends DbSynchronizerPluginBase {

  /**
   * {@inheritDoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function synchronize(int $id, int $timestamp, string $status, array $nodes, string $synchronize_type): bool {
    foreach ($nodes as $node) {
      // Determine synchronization type.
      switch ($synchronize_type) {
        case AggregationTableMappingForm::SYNCHRONIZE_TYPE_CLEAN_INSERT:
        case AggregationTableMappingForm::SYNCHRONIZE_TYPE_INSERT_ONLY:
          if ($node->isNew()) {
            $node->save();
          }
          break;

        case AggregationTableMappingForm::SYNCHRONIZE_TYPE_UPDATE_ONLY:
          if (!$node->isNew()) {
            $node->save();
          }
          break;

        case AggregationTableMappingForm::SYNCHRONIZE_TYPE_UPDATE_AND_INSERT:
          $node->save();
          break;
      }
    }
    return TRUE;
  }

}
