<?php

namespace Drupal\db_aggregator\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines db_synchronizer annotation object.
 *
 * @Annotation
 */
class DbSynchronizer extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
