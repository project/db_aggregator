<?php

namespace Drupal\db_aggregator;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for db_synchronizer plugins.
 */
abstract class DbSynchronizerPluginBase extends PluginBase implements DbSynchronizerInterface {

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

}
