<?php

namespace Drupal\db_aggregator;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an aggregation data source entity type.
 */
interface AggregationDataSourceInterface extends ConfigEntityInterface {

}
