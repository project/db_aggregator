<?php

namespace Drupal\db_aggregator\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\db_aggregator\AggregationDataSourceInterface;
use Drupal\db_aggregator\AggregationTableMappingInterface;

/**
 * Defines the aggregation table mapping entity type.
 *
 * @ConfigEntityType(
 *   id = "aggregation_table_mapping",
 *   label = @Translation("Aggregation table mapping"),
 *   label_collection = @Translation("Aggregation table mappings"),
 *   label_singular = @Translation("aggregation table mapping"),
 *   label_plural = @Translation("aggregation table mappings"),
 *   label_count = @PluralTranslation(
 *     singular = "@count aggregation table mapping",
 *     plural = "@count aggregation table mappings",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\db_aggregator\AggregationTableMappingListBuilder",
 *     "form" = {
 *       "add" = "Drupal\db_aggregator\Form\AggregationTableMappingForm",
 *       "edit" = "Drupal\db_aggregator\Form\AggregationTableMappingForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "aggregation_table_mapping",
 *   admin_permission = "administer aggregation_table_mapping",
 *   links = {
 *     "collection" = "/admin/structure/aggregation-table-mapping",
 *     "add-form" = "/admin/structure/aggregation-table-mapping/add",
 *     "edit-form" = "/admin/structure/aggregation-table-mapping/{aggregation_table_mapping}",
 *     "delete-form" = "/admin/structure/aggregation-table-mapping/{aggregation_table_mapping}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "datasource",
 *     "contentType",
 *     "sourceTrackingField",
 *     "mappings",
 *     "synchronizer",
 *     "synchronyTime",
 *     "synchronyType",
 *     "description"
 *   }
 * )
 */
class AggregationTableMapping extends ConfigEntityBase implements AggregationTableMappingInterface {

  /**
   * The aggregation table mapping ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The aggregation table mapping label.
   *
   * @var string
   */
  protected $label;

  /**
   * The aggregation table mapping datasource.
   *
   * @var string
   */
  protected $datasource;

  /**
   * The aggregation table mapping content type.
   *
   * @var string
   */
  protected $contentType;

  /**
   * The aggregation table mapping source tracking field.
   *
   * @var string
   */
  protected $sourceTrackingField;

  /**
   * The aggregation table mapping mappings.
   *
   * @var array
   */
  protected $mappings;

  /**
   * The aggregation table mapping synchronizer.
   *
   * @var string
   */
  protected $synchronizer;

  /**
   * The aggregation table mapping synchronyTime.
   *
   * @var string
   */
  protected $synchronyTime;

  /**
   * The aggregation table mapping synchronyType.
   *
   * @var string
   */
  protected $synchronyType;

  /**
   * The aggregation table mapping status.
   *
   * @var bool
   */
  protected $status;

  /**
   * The aggregation_table_mapping description.
   *
   * @var string
   */
  protected $description;

  /**
   * The datasource entity object.
   *
   * @var AggregationDataSourceInterface|NULL
   */
  protected $datasourceObject = NULL;

  /**
   * Set a datasource entity object.
   *
   * @return \Drupal\db_aggregator\Entity\AggregationDataSource
   */
  public function getDataSource() {
    if (!($this->datasourceObject instanceof AggregationDataSourceInterface)) {
      $this->datasourceObject = AggregationDataSource::load($this->datasource);
    }
    return $this->datasourceObject;
  }

  /**
   * Set a new datasource entity object.
   *
   * @param \Drupal\db_aggregator\Entity\AggregationDataSource $data_source
   */
  public function setDataSource(AggregationDataSource $data_source) {
    $this->datasourceObject = $data_source;
  }
}
