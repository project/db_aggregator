<?php

namespace Drupal\db_aggregator\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\db_aggregator\AggregationDataSourceInterface;

/**
 * Defines the aggregation data source entity type.
 *
 * @ConfigEntityType(
 *   id = "aggregation_data_source",
 *   label = @Translation("Aggregation data source"),
 *   label_collection = @Translation("Aggregation data sources"),
 *   label_singular = @Translation("Aggregation data source"),
 *   label_plural = @Translation("Aggregation data sources"),
 *   label_count = @PluralTranslation(
 *     singular = "@count aggregation data source",
 *     plural = "@count aggregation data sources",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\db_aggregator\AggregationDataSourceListBuilder",
 *     "form" = {
 *       "add" = "Drupal\db_aggregator\Form\AggregationDataSourceForm",
 *       "edit" = "Drupal\db_aggregator\Form\AggregationDataSourceForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "aggregation_data_source",
 *   admin_permission = "administer aggregation_data_source",
 *   links = {
 *     "collection" = "/admin/structure/aggregation-data-source",
 *     "add-form" = "/admin/structure/aggregation-data-source/add",
 *     "edit-form" = "/admin/structure/aggregation-data-source/{aggregation_data_source}",
 *     "delete-form" = "/admin/structure/aggregation-data-source/{aggregation_data_source}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "type",
 *     "database",
 *     "table",
 *     "primaryKey",
 *     "timestampField",
 *     "statusField",
 *     "username",
 *     "password",
 *     "prefix",
 *     "host",
 *     "port",
 *     "status",
 *     "description"
 *   }
 * )
 */
class AggregationDataSource extends ConfigEntityBase implements AggregationDataSourceInterface {

  /**
   * The aggregation data source ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The aggregation data source label.
   *
   * @var string
   */
  protected $label;

  /**
   * The aggregation data source database type.
   *
   * @var string
   */
  protected $type;

  /**
   * The aggregation data source database name.
   *
   * @var string
   */
  protected $database;

  /**
   * The aggregation data source table name.
   *
   * @var string
   */
  protected $table;

  /**
   * The aggregation data source primary key.
   *
   * @var string
   */
  protected $primaryKey;

  /**
   * The aggregation data source timestamp field.
   *
   * @var string
   */
  protected $timestampField;

  /**
   * The aggregation data source status field.
   *
   * @var string
   */
  protected $statusField;

  /**
   * The aggregation data source username.
   *
   * @var string
   */
  protected $username;

  /**
   * The aggregation data source password.
   *
   * @var string
   */
  protected $password;

  /**
   * The aggregation data source table prefix.
   *
   * @var string
   */
  protected $prefix;

  /**
   * The aggregation data source host.
   *
   * @var string
   */
  protected $host;

  /**
   * The aggregation data source port.
   *
   * @var string
   */
  protected $port;

  /**
   * The aggregation data source status.
   *
   * @var bool
   */
  protected $status;

  /**
   * The aggregation_data_source description.
   *
   * @var string
   */
  protected $description;

}
