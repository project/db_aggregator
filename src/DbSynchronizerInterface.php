<?php

namespace Drupal\db_aggregator;

/**
 * Interface for db_synchronizer plugins.
 */
interface DbSynchronizerInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label(): string;

  /**
   * Synchronize a row of data.
   *
   * @param int $id
   *   Primary key of this row.
   * @param int $timestamp
   *   Timestamp of this row, NULL if it does not define in data source.
   * @param string $status
   *   Status of this row, NULL if it does not define in data source.
   * @param \Drupal\node\NodeInterface[] $nodes
   *   Nodes new or exist.
   * @param string $synchronize_type
   *   Type.
   *
   * @return bool
   *   If successfully.
   */
  public function synchronize(int $id, int $timestamp, string $status, array $nodes, string $synchronize_type): bool;

}
