<?php

namespace Drupal\db_aggregator;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an aggregation table mapping entity type.
 */
interface AggregationTableMappingInterface extends ConfigEntityInterface {

}
