<?php

namespace Drupal\Tests\db_aggregator\Functional;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\db_aggregator\Entity\AggregationTableMapping;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\BrowserTestBase;

include_once __DIR__ . '/../../../synchronize.inc';

/**
 * Test synchronize data from external databases.
 *
 * @group db_aggregator
 */
class SynchronizeTest extends BrowserTestBase {

  /**
   * Table name of testing mocking.
   */
  const MOCKING_TABLE = 'mocking_test';

  /**
   * Table name of testing mocking kyxm.
   */
  const ORACLE_MOCKING_TABLE_KYXM = 'kyxm';

  /**
   * Table name of testing mocking yfht.
   */
  const ORACLE_MOCKING_TABLE_YFHT = 'yfht';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['db_aggregator_test'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    // Setup oracle connection.
    $oracle_database = include __DIR__ . '/oracle_connection.inc';
    Database::addConnectionInfo('oracle', 'db_aggregator_test', $oracle_database);
    // Setup oracle connection.
    $mysql_database = include __DIR__ . '/mysql_connection.inc';
    Database::addConnectionInfo('mysql', 'db_aggregator_test', $mysql_database);
    parent::setUp();
  }

  /**
   * {@inheritdoc}
   */
  protected function tearDown() {
    // Clear external database data which was created
    // in db_aggregator_test_install()
    // when db_aggregator_test module was installed.
    db_aggregator_test_uninstall();
    parent::tearDown();
  }

  /**
   * Test Content type for test purple.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  public function testAllNeedToTest() {
    $this->testContentType();
    $this->testMockingTable();
    $this->testPrepareOperations();
  }

  /**
   * Test Content type for test purple.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function testContentType() {
    $node_type = NodeType::load('db_aggregator_test');
    $this->assertInstanceOf(NodeType::class, $node_type);

    $node = Node::create([
      'type' => 'db_aggregator_test',
      'title' => 'test',
      'body' => ['value' => 'test content'],
      'field_source_id' => '12',
    ]);
    $node->save();
    $this->assertInstanceOf(Node::class, $node);
    $node->delete();
  }

  /**
   * Test mocking table data rows.
   *
   * @throws \Exception
   */
  private function testMockingTable() {
    $this->testDataCount(Database::getConnection('db_aggregator_test', 'mysql'));
    $this->testDataCount(Database::getConnection('db_aggregator_test', 'oracle'));
  }

  /**
   * Test how many rows in the tables.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   Where is the test tables.
   */
  private function testDataCount(Connection $connection) {
    $rs = $connection->select(static::MOCKING_TABLE)
      ->countQuery()
      ->execute()
      ->fetchField();
    $this->assertEquals(2000, $rs);
    $rs = $connection->select(static::ORACLE_MOCKING_TABLE_KYXM)
      ->countQuery()
      ->execute()
      ->fetchField();
    $this->assertEquals(2000, $rs);
    $rs = $connection->select(static::ORACLE_MOCKING_TABLE_YFHT)
      ->countQuery()
      ->execute()
      ->fetchField();
    $this->assertEquals(2000, $rs);
  }

  /**
   * Test prepare operations.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function testPrepareOperations() {
    $mysql_database = include __DIR__ . '/mysql_connection.inc';
    $this->testPrepareForConnection('mysql', $mysql_database);
    $oracle_database = include __DIR__ . '/oracle_connection.inc';
    $this->testPrepareForConnection('oracle', $oracle_database);
  }

  /**
   * Test per connection.
   *
   * @param string $database_type
   *   Oracle or MySQL.
   * @param array $connection
   *   Where is the test tables.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function testPrepareForConnection(string $database_type, array $connection) {
    $table_mapping = $this->overrideDatabaseConnection(AggregationTableMapping::load($database_type . '_mocking'), $connection);
    $operations = _db_aggregator_prepare_operations_for_batch($table_mapping);
    $this->assertEquals(2, count($operations));
    $this->doSynchronization($operations, $table_mapping);

    $table_mapping = $this->overrideDatabaseConnection(AggregationTableMapping::load($database_type . '_yfht'), $connection);
    $operations = _db_aggregator_prepare_operations_for_batch($table_mapping);
    $this->assertEquals(2, count($operations));
    $this->doSynchronization($operations, $table_mapping);

    $table_mapping = $this->overrideDatabaseConnection(AggregationTableMapping::load($database_type . '_kyxm'), $connection);
    $operations = _db_aggregator_prepare_operations_for_batch($table_mapping);
    $this->assertEquals(2, count($operations));
    $this->doSynchronization($operations, $table_mapping);
  }

  /**
   * Test synchronization from MySQL database.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function doSynchronization($operations, AggregationTableMapping $table_mapping) {
    $ids = [];
    foreach ($operations as $operation) {
      $ids = array_merge($ids, $operation[1][1]);
      call_user_func_array($operation[0], $operation[1]);
    }
    $nodes = Node::loadMultiple($ids);
    $data_source = $table_mapping->getDataSource();

    $connection = synchronize_connection($data_source);
    foreach ($nodes as $node) {
      $this->assertInstanceOf(Node::class, $node);
      $this->assertEquals('db_aggregator_test', $node->bundle());

      $source_id = $node->get($table_mapping->get('sourceTrackingField'))->offsetGet(0)->get('value')->getValue();
      $source_id = str_replace($table_mapping->id() . ':', '', $source_id);
      $rs = $connection->select($data_source->get('table'), 't')
        ->fields('t', synchronize_get_datasource_fields($table_mapping))
        ->condition($data_source->get('primaryKey'), $source_id)
        ->execute()
        ->fetchAllAssoc($data_source->get('primaryKey'), \PDO::FETCH_ASSOC);

      $this->assertEquals(1, count($rs));
      $row = reset($rs);
      $mappings = $table_mapping->get('mappings');
      try {
        foreach ($mappings as $mapping) {
          $this->assertEquals($row[$mapping['sourceField']], $node->get($mapping['targetField'])->offsetGet(0)->get($mapping['targetProperty'])->getValue());
        }
      } finally {
        $node->delete();
      }
    }
  }

  /**
   * Override database connection config from test env.
   *
   * @param \Drupal\db_aggregator\Entity\AggregationTableMapping $table_mapping
   *   The mapping.
   * @param array $config
   *   The env config.
   *
   * @return \Drupal\db_aggregator\Entity\AggregationTableMapping
   *   The mapping with datasource object which is overrided.
   */
  private function overrideDatabaseConnection(AggregationTableMapping $table_mapping, array $config): AggregationTableMapping {
    $datasource = $table_mapping->getDataSource();
    $datasource->set('database', $config['database']);
    $datasource->set('host', $config['host']);
    $datasource->set('port', $config['port']);
    $datasource->set('prefix', $config['prefix']);
    $datasource->set('username', $config['username']);
    $datasource->set('password', $config['password']);
    $table_mapping->setDataSource($datasource);
    return $table_mapping;
  }

}
