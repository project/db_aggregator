<?php

/**
 * @file
 * External Oracle database.
 */

return [
  'database' => '',
  'username' => '',
  'password' => '',
  'prefix' => '',
  'host' => '',
  'port' => '1521',
  'namespace' => 'Drupal\\oracle\\Driver\\Database\\oracle',
  'driver' => 'oracle',
];
