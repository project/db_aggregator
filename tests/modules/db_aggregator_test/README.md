# Db aggregator test

This module should not be installed,
unless you are developer and want to make tests.

### Steps to set up this module

1. Prepare a oracle 19c database instance.

2. Create the following tables manually
   into the oracle database instance.

```sql
-- auto-generated definition
create table KYXM
(
    ID             NUMBER generated as identity,
    KYXMMC         VARCHAR2(255) not null,
    KYXMID         VARCHAR2(255) not null,
    KYXMLB         VARCHAR2(255) not null,
    KYXMFZRJZGH    VARCHAR2(255) not null,
    KYXMFZRXM      VARCHAR2(255) not null,
    KYXMBH         VARCHAR2(255) not null,
    DC_DCFIELDAUD  VARCHAR2(255),
    DC_DCDATADATE  VARCHAR2(255),
    DC_DCTIMESTAMP NUMBER        not null
)
/

create unique index KYXM_ID_UINDEX
    on KYXM (ID)
/

alter table KYXM
    add constraint KYXM_PK
        primary key (ID)
/


```

```sql
-- auto-generated definition
create table YFHT
(
    ID             NUMBER generated as identity,
    YFHTMC         VARCHAR2(255) not null,
    YFHEMH         VARCHAR2(255) not null,
    YFHTID         VARCHAR2(255) not null,
    YFHTZT         VARCHAR2(255) not null,
    DC_DCFIELDAUD  VARCHAR2(255),
    DC_DCDATADATE  VARCHAR2(255),
    DC_DCTIMESTAMP NUMBER        not null
)
/

create unique index YFHT_ID_UINDEX
    on YFHT (ID)
/

alter table YFHT
    add constraint YFHT_PK
        primary key (ID)
/


```

3. Add the oracle database instance connection config to drupal's `settings.php`.
```php
$databases['oracle']['db_aggregator_test'] = [
  'database' => '',
  'username' => '',
  'password' => '',
  'prefix' => '',
  'host' => '',
  'port' => '1521',
  'namespace' => 'Drupal\\oracle\\Driver\\Database\\oracle',
  'driver' => 'oracle',
];
```

4. Enable `db_aggregator_test` module, the installation will automatically insert rows
   into the tables above created for test.

### What you will get with this module is installed

1. A content type `db_aggregator_test`, with three main fields:
  - `title` Normal node title field.
  - `body` Normal node body field.
  - `field_source_id` Filed to store the identifier of the source data row.

2. A MySQL table `oracle_mocking` with 5000 records.
3. A Oracle table `KYXM` with 3000 records.
4. A Oracle table `YFHT` with 2000 records.
5. 3 aggregation_data_source config entities.
6. 3 aggregation_table_mapping config entities.

### How to test

1. Modify the `Oracle KYXM` and `Oracle YFHT` datasource entity,
   adjust the database connection info to the options you have set in `setting.php`.
2. Edit one of the table mapping entities, click `Save and Synchronize immediately`.
