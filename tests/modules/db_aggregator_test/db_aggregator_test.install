<?php

/**
 * @file
 * Install, update and uninstall functions for the DB Aggregator module.
 */

use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Database\Database;
use Drupal\Component\Utility\Random;
use Drupal\oracle\Driver\Database\oracle\Install\OracleInitial;
use Drupal\Tests\db_aggregator\Functional\SynchronizeTest;

const MOCKING_TABLE = SynchronizeTest::MOCKING_TABLE;
const ORACLE_MOCKING_TABLE_KYXM = SynchronizeTest::ORACLE_MOCKING_TABLE_KYXM;
const ORACLE_MOCKING_TABLE_YFHT = SynchronizeTest::ORACLE_MOCKING_TABLE_YFHT;

/**
 * Implements hook_install().
 */
function db_aggregator_test_install() {
  $connection = Database::getConnection('db_aggregator_test', 'mysql');
  foreach (_db_aggregator_test_schema() as $name => $definition) {
    // @todo Make oracle tables to be created automatically,
    //   once oracle driver 3.0.x is ready.
    $connection->schema()->createTable($name, $definition);
  }
  _db_aggregator_test_generate_test_data($connection);

  // Initial test data for oracle database.
  $connection = Database::getConnection('db_aggregator_test', 'oracle');
  $initial = new OracleInitial($connection);
  $initial->initial();
  foreach (_db_aggregator_test_schema() as $name => $definition) {
    // @todo Make oracle tables to be created automatically,
    //   once oracle driver 3.0.x is ready.
    $connection->schema()->createTable($name, $definition);
  }
  _db_aggregator_test_generate_test_data($connection);
}

/**
 * Generate test data.
 */
function _db_aggregator_test_generate_test_data(Connection $connection) {
  $query = $connection
    ->insert(MOCKING_TABLE)
    ->fields(['field1', 'field2', 'timestamp']);
  $random = new Random();
  for ($i = 1; $i <= 2000; $i++) {
    $query->values([
      'field1' => $random->sentences(3),
      'field2' => $random->sentences(3),
      'timestamp' => \Drupal::time()->getRequestTime(),
    ]);
    if ($i === 1000 || $i === 2000) {
      $query->execute();
    }
  }

  $query = $connection
    ->insert(ORACLE_MOCKING_TABLE_YFHT)
    ->fields(['YFHTMC', 'YFHEMH', 'YFHTID', 'YFHTZT',
      'DC_DCFIELDAUD', 'DC_DCDATADATE', 'DC_DCTIMESTAMP',
    ]);
  $random = new Random();
  for ($i = 1; $i <= 2000; $i++) {
    $query->values([
      'YFHTMC' => $random->sentences(3),
      'YFHTID' => $random->sentences(3),
      'YFHTZT' => $random->sentences(3),
      'YFHEMH' => $random->sentences(3),
      'DC_DCFIELDAUD' => 'U',
      'DC_DCDATADATE' => (new DrupalDateTime())->format('Ymd'),
      'DC_DCTIMESTAMP' => \Drupal::time()->getRequestTime(),
    ]);
    if ($i === 1000 || $i === 2000) {
      $query->execute();
    }
  }

  $query = $connection
    ->insert(ORACLE_MOCKING_TABLE_KYXM)
    ->fields(['KYXMMC', 'KYXMID', 'KYXMLB', 'KYXMFZRJZGH', 'KYXMFZRXM', 'KYXMBH',
      'DC_DCFIELDAUD', 'DC_DCDATADATE', 'DC_DCTIMESTAMP',
    ]);
  $random = new Random();
  for ($i = 1; $i <= 2000; $i++) {
    $query->values([
      'KYXMMC' => $random->sentences(3),
      'KYXMID' => $random->sentences(3),
      'KYXMLB' => $random->sentences(3),
      'KYXMFZRJZGH' => $random->sentences(3),
      'KYXMFZRXM' => $random->sentences(3),
      'KYXMBH' => $random->sentences(3),
      'DC_DCFIELDAUD' => 'U',
      'DC_DCDATADATE' => (new DrupalDateTime())->format('Ymd'),
      'DC_DCTIMESTAMP' => \Drupal::time()->getRequestTime(),
    ]);
    if ($i === 1000 || $i === 2000) {
      $query->execute();
    }
  }
}

/**
 * Implements hook_uninstall().
 */
function db_aggregator_test_uninstall() {
  $connection = Database::getConnection('db_aggregator_test', 'mysql');
  db_aggregator_test_drop_test_tables($connection);
  $connection = Database::getConnection('db_aggregator_test', 'oracle');
  db_aggregator_test_drop_test_tables($connection);
}

/**
 * Drop test tables by a given database connection.
 *
 * @param \Drupal\Core\Database\Connection $connection
 *   Where is the test tables.
 */
function db_aggregator_test_drop_test_tables(Connection $connection) {
  if ($connection->schema()->tableExists(MOCKING_TABLE)) {
    $connection->schema()->dropTable(MOCKING_TABLE);
  }
  if ($connection->schema()->tableExists(ORACLE_MOCKING_TABLE_YFHT)) {
    $connection->schema()->dropTable(ORACLE_MOCKING_TABLE_YFHT);
  }
  if ($connection->schema()->tableExists(ORACLE_MOCKING_TABLE_KYXM)) {
    $connection->schema()->dropTable(ORACLE_MOCKING_TABLE_KYXM);
  }
}

/**
 * Tha actual schema structure.
 */
function _db_aggregator_test_schema(): array {
  $schema[MOCKING_TABLE] = [
    'description' => 'Oracle table mocking for code test.',
    'fields' => [
      'id' => [
        'description' => 'primary key of this table.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'field1' => [
        'description' => 'normal field contain string value.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'field2' => [
        'description' => 'another normal field contain string value.',
        'type' => 'text',
        'oracle_type' => 'clob',
        'not null' => TRUE,
      ],
      'timestamp' => [
        'description' => 'last modification time to this record.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'last_activity' => [
        'description' => 'What last activity is acted on this row.',
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
        'default' => '',
      ],
    ],
    'primary key' => [
      'id',
    ],
    'indexes' => [
      'timestamp' => [
        'timestamp',
      ],
      'last_activity' => [
        'last_activity',
      ],
    ],
  ];
  $schema[ORACLE_MOCKING_TABLE_KYXM] = [
    'description' => 'Oracle table mocking for yhft.',
    'fields' => [
      'id' => [
        'description' => 'primary key of this table.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'KYXMMC' => [
        'description' => 'normal field contain string value.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'KYXMID' => [
        'description' => 'normal field contain string value.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'KYXMLB' => [
        'description' => 'normal field contain string value.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'KYXMFZRJZGH' => [
        'description' => 'normal field contain string value.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'KYXMFZRXM' => [
        'description' => 'normal field contain string value.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'KYXMBH' => [
        'description' => 'normal field contain string value.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'DC_DCFIELDAUD' => [
        'description' => 'What last activity is acted on this row.',
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
        'default' => '',
      ],
      'DC_DCDATADATE' => [
        'description' => 'What last activity is acted on this row.',
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
        'default' => '',
      ],
      'DC_DCTIMESTAMP' => [
        'description' => 'last modification time to this record.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
    ],
    'primary key' => [
      'id',
    ],
    'indexes' => [
      'DC_DCDATADATE' => [
        'DC_DCDATADATE',
      ],
      'DC_DCTIMESTAMP' => [
        'DC_DCTIMESTAMP',
      ],
    ],
  ];
  $schema[ORACLE_MOCKING_TABLE_YFHT] = [
    'description' => 'Oracle table mocking for kyxm.',
    'fields' => [
      'id' => [
        'description' => 'primary key of this table.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'YFHTMC' => [
        'description' => 'normal field contain string value.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'YFHEMH' => [
        'description' => 'normal field contain string value.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'YFHTID' => [
        'description' => 'normal field contain string value.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'YFHTZT' => [
        'description' => 'normal field contain string value.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'DC_DCFIELDAUD' => [
        'description' => 'What last activity is acted on this row.',
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
        'default' => '',
      ],
      'DC_DCDATADATE' => [
        'description' => 'What last activity is acted on this row.',
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
        'default' => '',
      ],
      'DC_DCTIMESTAMP' => [
        'description' => 'last modification time to this record.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
    ],
    'primary key' => [
      'id',
    ],
    'indexes' => [
      'DC_DCDATADATE' => [
        'DC_DCDATADATE',
      ],
      'DC_DCTIMESTAMP' => [
        'DC_DCTIMESTAMP',
      ],
    ],
  ];
  return $schema;
}
