# Db Aggregator

Provide a tool to aggregate data
from relational database
into drupal as nodes.

Both support cron automatically or manually aggregation.

Currently, only support MySQL and Oracle as datasource.

## How to use

1. Add a datasource in `/admin/structure/aggregation-data-source`.
2. Add a table mapping in `/admin/structure/aggregation-table-mapping`.
3. Set how many records will process per batch in `https://www.drupalcode.cn/admin/config/system/db-aggregator`.
4. Enjoy it.
